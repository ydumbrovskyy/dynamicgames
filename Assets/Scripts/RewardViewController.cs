﻿using UnityEngine;

public class RewardViewController : MonoBehaviour
{
    #region UI Components
    [SerializeField]
    private UIInput playersCountInput;
    [SerializeField]
    private UIInput placeInput;
    [SerializeField]
    private UIInput maximumInput;
    [SerializeField]
    private UIInput minimumInput;

    [SerializeField]
    private UILabel resultLabel;

    [SerializeField]
    private UILabel errorLabel;
    #endregion UI Components

    //Error messages
    private static readonly string msg_PersonsNumberWrong = "Number of person should be more then 1";
    private static readonly string msg_PlaceWrong = "Place should be more then 0 and less or equals persons number";
    private static readonly string msg_MaximumWrong = "Maximum should be more then 0 and minimum";
    private static readonly string msg_MinimumNumberWrong = "Minimum should be more then 0 and less or equals maximm";

    //UI messages
    private static readonly string msg_Result = "Player in {0} place won {1} coins";

    /// <summary>
    /// </summary>
    /// <returns></returns>
    int CalculateReward(float playersCount, float place, float maximum, float minimum)
    {
        float result = maximum - (maximum - minimum) / (playersCount - 1) * (place - 1);
        return (int)Mathf.Ceil(result);
    }

    /// <summary>
    /// Handling of click button "Calculate". Checking all Input components and if something wrong displaying message to errorLabel.
    /// After that, call CalculateReward and display result to resultLabel.
    /// </summary>
    public void OnCalculateBtnClicked()
    {
        float playersCount;
        float place;
        float maximum;
        float minimum;

        errorLabel.text = "";

        if (!float.TryParse(playersCountInput.value, out playersCount) || playersCount <= 1)
        {
            errorLabel.text = msg_PersonsNumberWrong;
            return;
        }

        if (!float.TryParse(placeInput.value, out place) || place <= 0 || place > playersCount)
        {
            errorLabel.text = msg_PlaceWrong;
            return;
        }

        if (!float.TryParse(maximumInput.value, out maximum) || maximum < 0)
        {
            errorLabel.text = msg_MaximumWrong;
            return;
        }

        if (!float.TryParse(minimumInput.value, out minimum) || minimum <= 0 || minimum > maximum)
        {
            errorLabel.text = msg_MinimumNumberWrong;
            return;
        }

        int result = CalculateReward(playersCount, place, maximum, minimum);
        resultLabel.text = string.Format(msg_Result, place, result);
    }
}